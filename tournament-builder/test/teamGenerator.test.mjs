import TeamGenerator from '../src/teamGenerator.mjs';
import { expect } from 'chai'; // Importez chai pour utiliser les assertions de style BDD

describe('TeamGenerator', () => {
  let players = ['Player1', 'Player2', 'Player3', 'Player4', 'Player5'];
  let playersPerTeam = 3;
  let teamGenerator;

  // beforeEach(() => {
  //   teamGenerator = new TeamGenerator(players, playersPerTeam);
  // });

  it('check there is still the good number of players after teams generated', () => {
    
    let players = ['Player1', 'Player2', 'Player3', 'Player4', 'Player5'];
    let playersPerTeam = 3;

    teamGenerator = new TeamGenerator(players, playersPerTeam);
    teamGenerator.generateTeams();
    const teams = teamGenerator.getTeams();

    let nbPlayers = 0
    players.forEach(player => {
      nbPlayers +=1
    })

    let nbPlayersInTeams = 0
    teams.forEach(team => {
      team.players.forEach( player =>{
        nbPlayersInTeams += 1
      })
    });

    expect(nbPlayers).to.equal(nbPlayersInTeams)
  });


  it('check if all players are in a team and not added several time', () => {
    
    let players = ['Player1', 'Player2', 'Player3', 'Player4', 'Player5'];
    let playersPerTeam = 3;

    teamGenerator = new TeamGenerator(players, playersPerTeam);
    teamGenerator.generateTeams();
    const teams = teamGenerator.getTeams();

    let playersChecked = []

    let errorRepeatedPlayers = []
    let errorNotAddedPlayers = []

    players.forEach((player) => {  
      console.log(`____________________________________________________${player}`)

      teams.forEach(team => {
        console.log(`_________________________________${team.name}`)

        team.players.forEach((teamPlayer) => {
          console.log('player: ', player)
          console.log('teamPlayer: ', teamPlayer)
          if(player === teamPlayer ){
            if(playersChecked.includes(teamPlayer)){
              errorRepeatedPlayers.push(player);
              console.log('error: ', errorRepeatedPlayers)
            }
            else{
              playersChecked.push(player)
              console.log('checked: ', playersChecked)
            }
          }
          if(!players.includes(teamPlayer)){
            console.log('includes: 0000000000000000000000000000000000000000000000000000',teamPlayer )
            console.log('errorNotAddedPlayers', errorNotAddedPlayers )
            errorNotAddedPlayers.push(teamPlayer)
          }
        })
      })
    });  
    expect(errorRepeatedPlayers).to.be.empty;
    expect(errorNotAddedPlayers).to.be.empty;
    expect(playersChecked).to.deep.equal(players);
  })


});