import fs from 'fs';
import csv from 'csv-parser';
// import CSVReader from '../csv/players.csv';
import TeamGenerator from './teamGenerator.mjs';
import TournamentGenerator from './tournamentGenerator.js';



// const csvReader = new CSVReader('../csv/players.csv');


const data = ["player_1","player_2","player_3","player_4","player_5","player_5","player_6","player_7","player_8","player_9","player_10","player_11","player_12","player_13","player_14","player_15","player_16","player_17","player_18","player_19","player_20","player_21","player_22","player_23","player_24","player_25","player_26"]

const teamGenerator = new TeamGenerator(data, 5);
teamGenerator.generateTeams();
const teams = teamGenerator.getTeams();
console.log('Équipes générées:', teams);
const tournamentGenerator = new TournamentGenerator(teams);

tournamentGenerator.generateTournament();

// Lecture du fichier CSV
// csvReader.readCSV()
//   .then(data => {
//     console.log('Données CSV:', data);

//     // Création d'une instance de TeamGenerator avec les données CSV
//     const teamGenerator = new TeamGenerator(data);

//     // Génération des équipes
//     teamGenerator.generateTeams();

//     // Obtention des équipes générées
//     const teams = teamGenerator.getTeams();
//     console.log('Équipes générées:', teams);

//     // Création d'une instance de TournamentGenerator avec les équipes générées
//     const tournamentGenerator = new TournamentGenerator(teams);

//     // Génération du tournoi
//     tournamentGenerator.generateTournament();
//   })
//   .catch(error => {
//     console.error('Erreur lors de la lecture du fichier CSV:', error);
//   });