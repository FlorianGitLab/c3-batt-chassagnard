class TeamGenerator {
  constructor(players, playersPerTeam = 3) {
    this.players = players;
    this.playersPerTeam = playersPerTeam;
    this.teams = [];
  }

  generateTeams() {
    let shuffledPlayers = [...this.players].sort(() => 0.5 - Math.random()); // Mélange aléatoire des joueurs
    let teamIndex = 0;

    while (shuffledPlayers.length > 0) {
      let teamPlayers = shuffledPlayers.splice(0, this.playersPerTeam);
      let teamName = `Équipe ${teamIndex + 1}`;
      let team = {
        name: teamName,
        players: teamPlayers,
      };
      this.teams.push(team);
      teamIndex++;
    }
  }

  getTeams() {
    return this.teams;
  }

  getTeamsTest() {
    // Copie de la liste des équipes existantes
    let teamsCopy = [...this.teams];
    
    // Ajout manuel d'une nouvelle équipe à la liste des équipes
    let manualTeam = {
      name: "Équipe test",
      players: ["Player1", "Player3", "Player5", "Player6", "Player7"] // Remplacez par les joueurs de votre choix
    };
    teamsCopy.push(manualTeam);
    
    // Retourne la liste mise à jour des équipes
    return teamsCopy;
  }
}

export default  TeamGenerator
// Exemple d'utilisation

